import React, { Component } from 'react';
import Registro from './Registro';
export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			edad: this.props.edad,
			palabra: ''
		};
		//this.handlerSumar = this.handlerSumar.bind(this);
	}
	obtenerPalabra = palabra => {
		this.setState({ palabra });
	};

	// handlerSumar() {
	// 	//console.log(this);
	// 	this.setState({ edad: this.state.edad + 1 });
	// }
	/**Optimizado */
	handlerSumar = () => {
		this.setState({ edad: this.state.edad + 1 });
	};
	render() {
		const { edad, palabra } = this.state;
		return (
			<div className="container">
				<br />
				<button
					// onClick={() => {
					// 	this.setState({ edad: edad + 1 });
					// }}
					onClick={this.handlerSumar}
					type="button"
					className="btn btn-primary"
				>
					Notifications <span className="badge badge-light">{edad}</span>
				</button>
				<div className="alert alert-success" role="alert">
					{palabra}
				</div>
				<br />
				<br />
				<Registro obtenerPalabra={this.obtenerPalabra} />
			</div>
		);
	}
}
